#!/bin/bash

set -e

function usage {
    echo "Usage: $0 [archive | plaintxt]"
    exit 1
}

function err_quit {
    echo "$1"
    exit 1
}


tempdir=_extracted
tempfile=_utf8_file.tex

function cleanup {
    rm -rf "$tempdir"
    rm -f $tempfile kindle_book.tex
    make clean
}

function sanitize {
    # remove embedded ads
    sed -i 's/.*http.*//g' $tempfile
    # replace/remove HTML entities
    sed -i 's/&ldquo;/“/g' $tempfile
    sed -i 's/&rdquo;/”/g' $tempfile
    sed -i 's/&mdash;/—/g' $tempfile
}


if [ "$#" -lt 1 ]; then
    usage
fi

trap cleanup EXIT

sourcefile=$1
filename=$(basename "$sourcefile")
extension="${filename##*.}"
filename="${filename%.*}"

if [ "$extension" == "zip" ]; then
    mkdir -p $tempdir
    unzip -qq $sourcefile -d $tempdir
    num_files=`ls -l $tempdir/*.txt | wc -l`
    if [ "$num_files" -ne 1 ]; then
        err_quit "requires one TXT file, but found $num_files"
    fi
    # how to do this correctly
    sourcefile=`basename $tempdir/*.txt`
    sourcefile="$tempdir/$sourcefile"

    filename=$(basename "$sourcefile")
    extension="${filename##*.}"
    filename="${filename%.*}"
elif [ "$extension" == "txt" ]; then
    :
else
    err_quit "unsupported extension"
fi

echo "filename: $filename"
echo "input a new name [optional]: "
read title
if [ "$title" == "" ]; then
    title=$filename
fi

file $sourcefile | grep "UTF-8" >/dev/null && isutf8=1 || isutf8=0
if [ "$isutf8" -ne 1 ]; then
    iconv -f gbk -t utf-8 $sourcefile > $tempfile
else
    ln -s $sourcefile $tempfile
fi
dos2unix -F $tempfile

sanitize

sed -i 's/^\(第.*章.*\)/\\clearpage\n\\section{\1}\n/g' $tempfile

# generate the PDF file
cp kindle_book.template kindle_book.tex
sed -i 's/CHAPTERS/'"${tempfile%.*}"'/g' kindle_book.tex
make

mv kindle_book.pdf $title.pdf

# elaborate the PDR file
# neither gawk nor writing to binary file is easy to grasp,
# so I use Python instead
python generate_pdr.py $title

exit 0
