LATEX=xelatex -halt-on-error

SOURCE=kindle_book.tex

all:
	@$(LATEX) $(SOURCE)
	@$(LATEX) $(SOURCE)

clean:
	@rm -f *.log *.aux *.toc *.out
