#!/usr/bin/env python
#coding: utf-8

from __future__ import print_function, division
import sys
import re
import struct
from pypinyin import lazy_pinyin, Style

def main():
    if len(sys.argv) < 2:
        print('Usage: {} [title]')
        sys.exit(1)

    pat = re.compile(r'{(\d+)}[^\ ]* (.*)}{(\d+)}')

    chapters = []
    
    f = open('kindle_book.toc', 'r')
    for line in f:
        mo = pat.search(line)
        if mo is None:
            print('illegal line in TOC: {}'.format(line))
            sys.exit(1)
        chapters.append(mo.groups())
    f.close()

    f = open(sys.argv[1] + '.pdr', 'wb')
    f.write(b'\xde\xad\xca\xbb\x01')
    f.write(struct.pack('>I', 0))
    f.write(struct.pack('>I', len(chapters)))
    f.write(b'\x00')

    for idx, chapter in enumerate(chapters):
        chapter_no, chapter_name, page = chapter
        chapter_no = int(chapter_no)
        page = int(page)
        f.write(struct.pack('>I', page-1))

        chapter_name_pinyin = u' '.join(lazy_pinyin(chapter_name.decode('utf-8'), style=Style.TONE3))
        try:
            _ = chapter_name_pinyin.encode('ascii')
        except UnicodeEncodeError:
            print(chapter_name, chapter_name_pinyin)
            chapter_name_pinyin = u''

        bookmark_name = '{: <8}{: <5}{}'.format(page, chapter_no, chapter_name_pinyin)
        f.write(struct.pack('>H', len(bookmark_name)))
        f.write(bookmark_name.encode('utf-8'))
        if idx < len(chapters) - 1:
            f.write(b'\x00')
    f.write(b'\x0a')
    f.close()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
        sys.exit(1)
